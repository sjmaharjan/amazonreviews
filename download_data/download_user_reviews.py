from datetime import datetime
import time
import re
import MySQLdb

__author__ = 'shrprasha'

from bs4 import BeautifulSoup
import urllib2


conn = MySQLdb.connect("localhost", "root", "", "ml_project_amazon_data", charset='utf8')
cursor = conn.cursor()

def get_review_links_for_pages(customer_id, start=1, end=100000):
    previous_page_first_reviewid = ""
    for page_no in range(start, end + 1):
        ## print user_reviews_url.format(customer_id=customer_id, page_no=page_no)
        previous_page_first_reviewid = get_user_reviews(user_reviews_url.format(customer_id=customer_id, page_no=page_no), previous_page_first_reviewid)
        if not previous_page_first_reviewid:
            break
        # try:
        #     previous_page_first_reviewid = get_user_reviews(
        #         user_reviews_url.format(customer_id=customer_id, page_no=page_no), previous_page_first_reviewid)
        #     if not previous_page_first_reviewid:
        #         return
        # # except:
        # except Exception, e:
        #     print e
        #     print "ERROR:", customer_id, page_no

def get_helpful_count(div):
    try:
        text = div.text.strip()
        if "people found the following review helpful" not in text:
            return 0, 0
        splitted = div.text.strip().split(" ", 3)
        helpful_count = splitted[0]
        out_of_helpful_count = splitted[2]
        return int(helpful_count), int(out_of_helpful_count)
    except:
        return 0, 0

def get_verified_purchase(div):
    return True if div.find("span", {"class": "crVerifiedStripe"}) else False

def get_user_reviews(user_reviews_url, previous_page_first_reviewid):
    while(True):
        # page = urllib2.urlopen("http://www.amazon.com/gp/cdp/member-reviews/A1S3C5OFU508P3?ie=UTF8&display=public&page=4&sort_by=MostRecentReview")
        try:
            page = urllib2.urlopen(user_reviews_url)
        except:
            time.sleep(2)
            continue
        print user_reviews_url
        soup = BeautifulSoup(page.read())
        # soup = BeautifulSoup(open("/Users/shrprasha/del/A3B0T6QS1XX0UL_2.html", "r").read())
        reviews_table = soup.findAll("table", {"border": "0", "cellpadding": "0", "cellspacing": "0", "width": "100%"})[-1]
        # print reviews_table
        reviews_trs = reviews_table.findAll("tr", recursive=False)
        # print len(reviews_trs)
        for i, tr in enumerate(reviews_trs):
            if (i+1) % 3 == 1:
                try:
                    product_id = tr.find("a")["href"].rsplit("/", 1)[-1]
                except:
                    product_id = None
                    print "Product does not exist", user_reviews_url
            elif (i+1) % 3 == 2:
                if not product_id:
                    continue
                review_td = tr.find("td")
                review_id = review_td.find("a")["name"]
                # for first reviewid, check for end of pages
                if i < 2:
                    if previous_page_first_reviewid == review_id:
                        return ""
                    else:
                        previous_page_first_reviewid = review_id

                review_div = review_td.find("div")
                review_divs = review_div.findAll("div", recursive=False)
                helpful_count, out_of_helpful_count = get_helpful_count(review_divs[0])
                if out_of_helpful_count > 0:
                    review_divs.pop(0)
                customer_review_rating = int(review_divs[0].find("span").find("img")["title"].strip()[0])
                review_title = review_divs[0].find("b").text
                # print "review_title", review_title
                review_written_date = datetime.strptime(review_divs[0].find("nobr").text.strip(), '%B %d, %Y').date()

                amazon_vine_program_review = True if review_div.findAll("a", {"href": "http://www.amazon.com/gp/vine/help"}) else False

                review_tiny_divs = review_div.findAll("div", {"class": "tiny"})
                amazon_verified_purchase = get_verified_purchase(review_tiny_divs[0])
                review_from_title = review_tiny_divs[-1].find("a").text

                review_text = review_div.find("div", {"class": "reviewText"}).text.strip()

                reg_str = r'Comment \((\d+)\)'
                comments = re.findall(reg_str, review_divs[-1].findAll("a")[1].text.strip())
                number_of_comments = int(comments[0]) if comments else 0

            elif (i+1) % 3 == 0:
                if not product_id:
                    continue
                ## print i, previous_page_first_reviewid, review_id
                # print review_id, product_id, customer_id, \
                #     helpful_count, out_of_helpful_count, customer_review_rating, review_title, review_written_date, \
                #     review_from_title, review_text, number_of_comments, amazon_verified_purchase, amazon_vine_program_review
                #place into db
                cursor.execute("""INSERT IGNORE INTO reviews(review_id, product_id, customer_id,
                                helpful_count, out_of_helpful_count, customer_review_rating, review_title, review_written_date,
                                review_from_title, review_text, number_of_comments, amazon_verified_purchase, amazon_vine_program_review)
                                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %r, %r)""",
                                (review_id, product_id, customer_id,
                                 helpful_count, out_of_helpful_count, customer_review_rating, review_title.encode('utf-8'), str(review_written_date),
                                review_from_title.encode('utf-8'), review_text.encode('utf-8'), number_of_comments, amazon_verified_purchase, amazon_vine_program_review)
                               )
            conn.commit()
        return previous_page_first_reviewid


def exists_in_db(user_reviews_url):
    try:
        page = urllib2.urlopen(user_reviews_url)
    except:
        time.sleep(2)
        get_user_reviews(user_reviews_url)
    soup = BeautifulSoup(page.read())
    reviews_table = soup.findAll("table", {"border": "0", "cellpadding": "0", "cellspacing": "0", "width": "100%"})[-1]
    # print reviews_table
    reviews_trs = reviews_table.findAll("tr", recursive=False)
    # print len(reviews_trs)
    for i, tr in enumerate(reviews_trs):
        if (i+1) % 3 == 1:
            product_id = tr.find("a")["href"].rsplit("/", 1)[-1]
        elif (i+1) % 3 == 2:
            review_td = tr.find("td")
            review_id = review_td.find("a")["name"]
            cursor.execute("select r.review_id "
                           "from reviews r "
                           "where r.review_id = %s ", review_id)
            result = cursor.fetchone()
            print review_id,
            if result:
                print "EXISTS"
            else:
                print "DOES NOT EXIST"


if __name__ == "__main__":
    # customer_ids = ["A445XMNE5IAA1", "A3TOT2MDXC3J4Y"] # short customers for testing
    # customer_ids = ["A3AKD6UWETAXGE", "A1V1LGKJMMAM30", "AE820S4DM51RK", "A33GGMOZAGLOX8", "A2F6N60Z96CAJI"]
    # customer_ids = ["A1V1LGKJMMAM30", "AE820S4DM51RK", "A33GGMOZAGLOX8", "A2F6N60Z96CAJI"]
    customer_ids = ["A2F6N60Z96CAJI"]
    for customer_id in customer_ids:
        print customer_id
        user_reviews_url = "http://www.amazon.com/gp/cdp/member-reviews/{customer_id}?ie=UTF8&display=public&page={page_no}&sort_by=MostRecentReview"
        get_review_links_for_pages(customer_id, start=30)

    # user_reviews_url = "http://www.amazon.com/gp/cdp/member-reviews/{customer_id}?ie=UTF8&display=public&page={page_no}&sort_by=MostRecentReview"
    # customer_id = "A1V1LGKJMMAM30"
    # page_no = 19
    #
    # # exists_in_db(user_reviews_url.format(customer_id=customer_id, page_no=page_no))
    #
    # get_user_reviews(user_reviews_url.format(customer_id=customer_id, page_no=page_no), "")

    conn.close()











    # # customer_id = "A3B0T6QS1XX0UL"
    # # page_no = 2
    # customer_id = "A1S3C5OFU508P3"
    # page_no = 4
