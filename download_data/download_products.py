import urllib2
from datetime import datetime
import time
import re
from bs4 import BeautifulSoup
import MySQLdb
import nltk
import requests
import traceback

__author__ = 'shrprasha'



conn = MySQLdb.connect("localhost", "root", "", "ml_project_amazon_data", charset='utf8')
cursor = conn.cursor()

# def get_product_ids_from_db():
#     # query = "SELECT distinct r.product_id from reviews r order by r.product_id"
#     query = "select distinct r.product_id from reviews r " \
#             "where not exists " \
#             "(select b.product_id from books b where r.product_id = b.product_id) " \
#             "and r.customer_id in ('A1V1LGKJMMAM30','A2F6N60Z96CAJI','A3AKD6UWETAXGE','AE820S4DM51RK')"
#     cursor.execute(query)
#     for result in cursor.fetchall():
#         yield result[0]

def get_product_ids_from_db():
    # query = "SELECT distinct r.product_id from reviews r order by r.product_id"
    query = "select distinct r.product_id from reviews r " \
            "where not exists " \
            "(select b.product_id from books b where r.product_id = b.product_id) " \
            "and r.customer_id='A2F6N60Z96CAJI' " \
            "order by r.product_id desc"
    # query = "select distinct r.product_id from reviews r " \
    #         "where not exists " \
    #         "(select b.product_id from books b where r.product_id = b.product_id)"
    cursor.execute(query)
    for result in cursor.fetchall():
        yield result[0]

def get_prices(price_swatch_div):
    prices = {'kindle': None, 'paperback': None, 'hardcover': None}
    price_spans = price_swatch_div.findAll("span", {"class": "a-button-inner"})
    for price_span in price_spans:
        splitted = price_span.text.replace("from", '').split()
        if len(splitted) < 3: # removing Audio CD
            # print splitted
            type, price = splitted
            type = type.lower()
            if type in prices:
                try:
                    prices[type] = float(price.strip("$"))
                except:
                    pass  # cases when - is present in the place of price
    return prices


def clean_genre_ranking(genre_ranking):
    genre_ranking = genre_ranking.replace('Amazon Best Sellers Rank:', '')
    genre_ranking = genre_ranking.replace('(See Top 100 in Books)', '')
    genre_ranking = re.sub(u'\..*}\n', '', genre_ranking)
    genre_ranking = re.sub(u'\n+', u'\n', genre_ranking)
    return genre_ranking


def get_publisher_date(product_details_lis):
    publication_date = None
    publisher = None
    size_info = None
    # print len(product_details_lis)
    for product_details_li in product_details_lis:
        txt = product_details_li.text
        # print txt
        if 'pages' in txt or 'KB' in txt:
            size_info = txt
        elif 'Publication Date' in txt:
            txt = txt.replace('Publication Date:', '').strip()
            publication_date = datetime.strptime(txt, '%B %d, %Y').date()
        elif 'Publisher:' in txt:
            publisher = txt.replace('Publisher:', '').strip()
    return size_info, publication_date, publisher

def get_product_details(product_url):
    print product_url
    while(True):
        try:
            page = urllib2.urlopen(product_url)
            break
        except urllib2.HTTPError as e:
            if e.getcode() == 404:
                print "NOT FOUND"
                return
            time.sleep(2)
            continue
    soup = BeautifulSoup(page.read(), 'html.parser')
    # page = urllib2.urlopen(product_url)
    # soup = BeautifulSoup(open("/Users/shrprasha/del/cc.html", "r").read(), 'html.parser')
    product_category = soup.find("div", {"id": "nav-subnav"}).find("a").find("span", {"class": "nav-a-content"}).text
    product_title = soup.find("span", {"id": "productTitle"}).text

    prices = get_prices(soup.find("div", {"id": "tmmSwatches"}))

    product_details_table = soup.find("table", {"id": "productDetailsTable"})
    product_details_lis = product_details_table.find("ul").findAll("li")
    size_info, publication_date, publisher = get_publisher_date(product_details_lis)
    type, size_size_units = size_info.split(":")
    size, size_units = size_size_units.split()

    # print size_info, publication_date, publisher

    genre_ranking = product_details_table.find("li", {"id": "SalesRank"}).text

    # page has broken html, so div cannot be addressed directly by id
    description = soup.find("div", {"id": "bookDescription_feature_div"}).find("div").text

    review_summary_div = soup.find("div", {"id": "revSum"})
    summary_stars_div = review_summary_div.find("div", {"id": "summaryStars"})
    average_customer_rating = float(summary_stars_div.find("a")["title"].split(" ", 1)[0])
    number_of_reviews = summary_stars_div.text.strip()

    star_ratings_trs = review_summary_div.find("table", {"id": "histogramTable"}).findAll("tr", recursive=False)
    percent_of_stars = [0.] * 5
    for star_ratings_tr in star_ratings_trs:
        stars, _, percent = star_ratings_tr.text.split()
        percent_of_stars[int(stars)-1] = int(percent[:-1])

    genre_ranking = clean_genre_ranking(genre_ranking)

    cursor.execute("""INSERT IGNORE INTO ml_project_amazon_data.books (product_id, product_category, product_title, hardcover_price,
                    number_of_reviews, average_customer_rating, percent_of_1star_reviews, percent_of_2star_reviews,
                    percent_of_3star_reviews, percent_of_4star_reviews, percent_of_5star_reviews, genre_ranking, description,
                    size, type, size_units, kindle_price, paperback_price, publication_date, publisher)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", (product_id
                    ,product_category, product_title, prices['hardcover']
                    ,number_of_reviews, average_customer_rating, percent_of_stars[0], percent_of_stars[1]
                    ,percent_of_stars[2], percent_of_stars[3], percent_of_stars[3], genre_ranking, description
                    ,size, type, size_units, str(prices['kindle']), str(prices['paperback'])
                    , str(publication_date), publisher
                    )
    )
    conn.commit()



if __name__ == "__main__":
    product_url = "http://www.amazon.com/gp/product/{product_id}"

    # product_id = "0062305859"
    # product_id = "B0182RBPSI"
    # product_id = "006000441X"
    # product_id = "0385737955"
    # product_id = "B00DTEKD60"
    # get_product_details(product_url.format(product_id=product_id))

    for product_id in get_product_ids_from_db():
        try:
            get_product_details(product_url.format(product_id=product_id))
        except Exception, e:
            print "ERROR HERE!!!"
            traceback.print_exc()

    conn.close()

