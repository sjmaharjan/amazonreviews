# -*- coding: utf-8 -*-
from __future__ import division
import csv
import nltk
from sklearn import preprocessing
from sklearn.base import BaseEstimator
from sklearn.feature_extraction.text import TfidfVectorizer
from collections import defaultdict
import numpy as np
# Lexical Featurs
import twokenize


class ReviewsNGramTfidfVectorizer(TfidfVectorizer):
    """
    Ngram Feature estimator
    """

    def build_analyzer(self):
        analyzer = super(TfidfVectorizer,
                         self).build_analyzer()
        return lambda doc: (w for w in analyzer(doc.review_title + " " + doc.review_text))


class BooksNGramTfidfVectorizer(TfidfVectorizer):
    """
    Ngram Feature estimator
    """

    def build_analyzer(self):
        analyzer = super(TfidfVectorizer,
                         self).build_analyzer()
        return lambda doc: (w for w in analyzer(doc.product.product_title + " " + doc.product.description))


class BooksGenreNGramTfidfVectorizer(TfidfVectorizer):
    """
    Ngram Feature estimator
    """

    def build_analyzer(self):
        analyzer = super(TfidfVectorizer,
                         self).build_analyzer()
        return lambda doc: (w for w in analyzer(doc.product.genre_ranking))


class POSTags(TfidfVectorizer):
    def build_analyzer(self):
        analyzer = super(TfidfVectorizer,
                         self).build_analyzer()
        return lambda doc: (w for w in analyzer(doc.pos_tag))


class ReviewsWritingDensityFeatures(BaseEstimator):
    """
    Writing density Feature estimator
    """

    def get_feature_names(self):
        return np.array(
            ['n_words', 'n_chars', 'exclamation', 'question', 'avgwordlenght', 'avesentencelength',
             'avgwordspersentence', 'allcaps'])

    def fit(self, documents, y=None):
        return self

    def transform(self, documents):
        exclamation = [doc.review_text.count('!') for doc in documents]
        # print exclamation
        question = [doc.review_text.count('?') for doc in documents]
        # print question

        n_words = [len(nltk.word_tokenize(doc.review_text)) for doc in documents]
        # print n_words
        n_chars = [len(doc.review_text) for doc in documents]

        avg_sent_length = [np.mean([len(sent) for sent in nltk.sent_tokenize(doc.review_text)]) for doc in documents]

        avg_word_lenght = [np.mean([len(word) for word in nltk.word_tokenize(doc.review_text)]) for doc in documents]

        avg_words_sentence = [np.mean([len(nltk.word_tokenize(sent)) for sent in nltk.sent_tokenize(doc.review_text)])
                              for
                              doc in documents]
        all_caps = [
            np.sum([word.isupper() for word in twokenize.tokenizeRawTweetText(doc.review_text) if len(word) > 2])
            for doc in documents]

        X = np.array(
            [n_words, n_chars, exclamation, question, avg_word_lenght, avg_sent_length, avg_words_sentence, all_caps]).T
        if not hasattr(self, 'scalar'):
            self.scalar = preprocessing.StandardScaler().fit(X)
        return self.scalar.transform(X)


class BookssWritingDensityFeatures(BaseEstimator):
    """
    Writing density Feature estimator
    """

    def get_feature_names(self):
        return np.array(
            ['n_words', 'n_chars', 'exclamation', 'question', 'avgwordlenght', 'avesentencelength',
             'avgwordspersentence', 'allcaps'])

    def fit(self, documents, y=None):
        return self

    def transform(self, documents):
        exclamation = [doc.product.description.count('!') for doc in documents]
        # print exclamation
        question = [doc.product.description.count('?') for doc in documents]
        # print question

        n_words = [len(nltk.word_tokenize(doc.product.description)) for doc in documents]
        # print n_words
        n_chars = [len(doc.product.description) for doc in documents]

        avg_sent_length = [np.mean([len(sent) for sent in nltk.sent_tokenize(doc.product.description)]) for doc in
                           documents]

        avg_word_lenght = [np.mean([len(word) for word in nltk.word_tokenize(doc.product.description)]) for doc in
                           documents]

        avg_words_sentence = [
            np.mean([len(nltk.word_tokenize(sent)) for sent in nltk.sent_tokenize(doc.product.description)]) for
            doc in documents]
        all_caps = [np.sum(
            [word.isupper() for word in twokenize.tokenizeRawTweetText(doc.product.description) if len(word) > 2])
                    for doc in documents]

        X = np.array(
            [n_words, n_chars, exclamation, question, avg_word_lenght, avg_sent_length, avg_words_sentence, all_caps]).T
        if not hasattr(self, 'scalar'):
            self.scalar = preprocessing.StandardScaler().fit(X)
        return self.scalar.transform(X)


# REF http://sentiwordnet.isti.cnr.it/code/SentiWordNetDemoCode.java
# REF Building Machine Learning Systems with Python Section Sentiment analysis
def load_sentiwordnet(path):
    scores = defaultdict(list)
    with open(path, "r") as csvfile:
        reader = csv.reader(csvfile, delimiter='\t', quotechar='"')
        for line in reader:
            # skip comments
            if line[0].startswith("#"):
                continue
            if len(line) == 1:
                continue
            POS, ID, PosScore, NegScore, SynsetTerms, Gloss = line
            if len(POS) == 0 or len(ID) == 0:
                continue
            # print POS,PosScore,NegScore,SynsetTerms
            for term in SynsetTerms.split(" "):
                # drop number at the end of every term
                term = term.split("#")[0]
                term = term.replace("-", " ").replace("_", " ")
                key = "%s/%s" % (POS, term.split("#")[0])
                scores[key].append((float(PosScore), float(NegScore)))
    for key, value in scores.iteritems():
        scores[key] = np.mean(value, axis=0)
    return scores


# REF Building Machine Learning Systems with Python Section Sentiment analysis
class SentiWordNetFeature(BaseEstimator):
    """
    Senti word net Feature estimator
    """

    def __init__(self):
        self.sentiwordnet = load_sentiwordnet('resources/SentiWordNet_3.0.0_20130122.txt')

    def get_feature_names(self):
        return np.array(['sent_neut', 'sent_pos', 'sent_neg', 'nouns', 'adjectives', 'verbs', 'adverbs'])

    def _get_sentiments(self, d):
        tagged_sent = d.review_pos_tagged
        pos_vals = []
        neg_vals = []
        nouns = 0.
        adjectives = 0.
        verbs = 0.
        adverbs = 0.
        for tag in tagged_sent.split():
            sent_len = 0
            try:
                t, p = tag.rsplit('/', 1)
            except ValueError as e:
                t,p="",""
            p_val, n_val = 0, 0
            sent_pos_type = None
            if p.startswith("NN"):
                sent_pos_type = "n"
                nouns += 1
            elif p.startswith("JJ"):
                sent_pos_type = "a"
                adjectives += 1
            elif p.startswith("VB"):
                sent_pos_type = "v"
                verbs += 1
            elif p.startswith("RB"):
                sent_pos_type = "r"
                adverbs += 1
            if sent_pos_type is not None:
                sent_word = "%s/%s" % (sent_pos_type, t.lower())
                if sent_word in self.sentiwordnet:
                    p_val, n_val = self.sentiwordnet[sent_word]
            pos_vals.append(p_val)
            neg_vals.append(n_val)
            sent_len += 1

        l = sent_len
        avg_pos_val = np.mean(pos_vals)
        avg_neg_val = np.mean(neg_vals)
        return [1 - avg_pos_val - avg_neg_val, avg_pos_val, avg_neg_val, nouns / l, adjectives / l, verbs / l,
                adverbs / l]

    def fit(self, documents, y=None):
        return self

    def transform(self, documents):
        X = np.array([self._get_sentiments(d) for d in documents])
        return X


class ReviewFeatures(BaseEstimator):
    """
    Review Feature estimator
    """

    def get_feature_names(self):
        return np.array(
            ['helpful_count', 'total_helpful_count', 'helpfulness_ratio', 'comments_count', 'amazon_verified',
             'amzaon_vine_review'])

    def fit(self, documents, y=None):
        return self

    def transform(self, reviews):
        helpful_count = [review.helpful_count for review in reviews]
        # print exclamation
        total_helpful_count = [review.out_of_helpful_count for review in reviews]
        # print question
        helpfulness_ratio = []
        for h_c, t_h_c in zip(helpful_count, total_helpful_count):
            if h_c == 0 or t_h_c:
                helpfulness_ratio.append(0.0)
            else:
                helpfulness_ratio.append(h_c / t_h_c)

        comments_count = [review.number_of_comments for review in reviews]

        amazon_verified = [review.number_of_comments for review in reviews]

        amzaon_vine_review = [review.number_of_comments for review in reviews]

        X = np.array([helpful_count, total_helpful_count, helpfulness_ratio, comments_count, amazon_verified,
                      amzaon_vine_review]).T
        if not hasattr(self, 'scalar'):
            self.scalar = preprocessing.StandardScaler().fit(X)
        return self.scalar.transform(X)


class BooksFeatures(BaseEstimator):
    """
    Books Feature estimator
    """

    def get_feature_names(self):
        return np.array(
            ['price', 'reviews_count', 'avg_cutomer_rating', 'one_stars', 'two_starts', 'three_stars', 'four_starts',
             'five_stars',
             'size'])

    def fit(self, documents, y=None):
        return self

    def transform(self, reviews):
        price, reviews_count, avg_cutomer_rating, one_stars, two_starts, three_stars, four_starts, five_stars, size = [], [], [], [], [], [], [], [], []
        for review in reviews:
            book = review.product

            price.append(book.hardcover_price if book.hardcover_price else 0.0)
            reviews_count.append(book.number_of_reviews if book.number_of_reviews else 0)
            avg_cutomer_rating.append(book.average_customer_rating if book.average_customer_rating else 0.0)
            one_stars.append(book.percent_of_1star_reviews if book.percent_of_1star_reviews else 0)
            two_starts.append(book.percent_of_2star_reviews if book.percent_of_2star_reviews else 0)
            three_stars.append(book.percent_of_3star_reviews if book.percent_of_3star_reviews else 0)
            four_starts.append(book.percent_of_4star_reviews if book.percent_of_4star_reviews else 0)
            five_stars.append(book.percent_of_5star_reviews if book.percent_of_5star_reviews else 0)
            size.append(book.size if book.size else 0)

        X = np.array([price, reviews_count, avg_cutomer_rating, one_stars, two_starts, three_stars,
                      four_starts, five_stars, size]).T
        if not hasattr(self, 'scalar'):
            self.scalar = preprocessing.StandardScaler().fit(X)
        return self.scalar.transform(X)
