# -*- coding: utf-8 -*-
from sklearn import grid_search
from sklearn.cross_validation import PredefinedSplit
from sklearn.linear_model import LinearRegression, Lasso, ElasticNet, Ridge, LogisticRegression
from sklearn.metrics import mean_squared_error, r2_score, classification_report, confusion_matrix, precision_score, \
    f1_score, recall_score
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.svm import LinearSVC, SVC
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import features
from twokenize import tokenizeRawTweetText
import numpy as np
import loader
import matplotlib.pyplot as plt
import math


def get_feature_extractor(f_name):
    """
    factory method to create feature
    :param f_name: feature name
    :return: feature object
    """
    # print f_name
    features_dic = dict(
        # Reviews
        # word ngram
        review_unigram=features.ReviewsNGramTfidfVectorizer(ngram_range=(1, 1), tokenizer=tokenizeRawTweetText,
                                                            analyzer="word",
                                                            lowercase=True),
        reviews_bigram=features.ReviewsNGramTfidfVectorizer(ngram_range=(2, 2), tokenizer=tokenizeRawTweetText,
                                                            analyzer="word",
                                                            lowercase=True),
        reviews_trigram=features.ReviewsNGramTfidfVectorizer(ngram_range=(3, 3), tokenizer=tokenizeRawTweetText,
                                                             analyzer="word",
                                                             lowercase=True),
        reviews_uni_bi_tri_gram=features.ReviewsNGramTfidfVectorizer(ngram_range=(1, 3), tokenizer=tokenizeRawTweetText,
                                                                     analyzer="word",
                                                                     lowercase=True),

        # char ngram
        reviews_char_bigram=features.ReviewsNGramTfidfVectorizer(ngram_range=(2, 2), tokenizer=tokenizeRawTweetText,
                                                                 analyzer="char",
                                                                 lowercase=True),
        reviews_char_tri=features.ReviewsNGramTfidfVectorizer(ngram_range=(3, 3), tokenizer=tokenizeRawTweetText,
                                                              analyzer="char",
                                                              lowercase=True),
        reviews_char_3_5=features.ReviewsNGramTfidfVectorizer(ngram_range=(3, 5), tokenizer=tokenizeRawTweetText,
                                                              analyzer="char",
                                                              lowercase=True),

        # char ngram word boundary
        reviews_char_bigram_word=features.ReviewsNGramTfidfVectorizer(ngram_range=(2, 2),
                                                                      tokenizer=tokenizeRawTweetText,
                                                                      analyzer="char_wb",
                                                                      lowercase=True),
        reviews_char_tri_word=features.ReviewsNGramTfidfVectorizer(ngram_range=(3, 3), tokenizer=tokenizeRawTweetText,
                                                                   analyzer="char_wb",
                                                                   lowercase=True),
        reviews_char_3_5_word=features.ReviewsNGramTfidfVectorizer(ngram_range=(3, 5), tokenizer=tokenizeRawTweetText,
                                                                   analyzer="char_wb",
                                                                   lowercase=True),

        # writing density
        review_writing_density=features.ReviewsWritingDensityFeatures(),

        # reviews
        review_info=features.ReviewFeatures(),

        # senti word net
        swn=features.SentiWordNetFeature(),

        # Books
        # word ngram
        books_unigram=features.BooksNGramTfidfVectorizer(ngram_range=(1, 1), tokenizer=tokenizeRawTweetText,
                                                         analyzer="word",
                                                         lowercase=True),
        books_bigram=features.BooksNGramTfidfVectorizer(ngram_range=(2, 2), tokenizer=tokenizeRawTweetText,
                                                        analyzer="word",
                                                        lowercase=True),
        books_trigram=features.BooksNGramTfidfVectorizer(ngram_range=(3, 3), tokenizer=tokenizeRawTweetText,
                                                         analyzer="word",
                                                         lowercase=True),
        books_uni_bi_tri_gram=features.BooksNGramTfidfVectorizer(ngram_range=(1, 3), tokenizer=tokenizeRawTweetText,
                                                                 analyzer="word",
                                                                 lowercase=True),

        # char ngram
        books_char_bigram=features.BooksNGramTfidfVectorizer(ngram_range=(2, 2), tokenizer=tokenizeRawTweetText,
                                                             analyzer="char",
                                                             lowercase=True),
        books_char_tri=features.BooksNGramTfidfVectorizer(ngram_range=(3, 3), tokenizer=tokenizeRawTweetText,
                                                          analyzer="char",
                                                          lowercase=True),
        books_char_3_5=features.BooksNGramTfidfVectorizer(ngram_range=(3, 5), tokenizer=tokenizeRawTweetText,
                                                          analyzer="char",
                                                          lowercase=True),

        # char ngram word boundary
        books_char_bigram_word=features.BooksNGramTfidfVectorizer(ngram_range=(2, 2), tokenizer=tokenizeRawTweetText,
                                                                  analyzer="char_wb",
                                                                  lowercase=True),
        books_char_tri_word=features.BooksNGramTfidfVectorizer(ngram_range=(3, 3), tokenizer=tokenizeRawTweetText,
                                                               analyzer="char_wb",
                                                               lowercase=True),
        books_char_3_5_word=features.BooksNGramTfidfVectorizer(ngram_range=(3, 5), tokenizer=tokenizeRawTweetText,
                                                               analyzer="char_wb",
                                                               lowercase=True),

        # writing density
        books_writing_density=features.BookssWritingDensityFeatures(),

        # books
        books_info=features.BooksFeatures(),

        # genre
        books_genre=features.BooksGenreNGramTfidfVectorizer(ngram_range=(1, 1),
                                                            analyzer="word",
                                                            lowercase=True),

    )

    return features_dic[f_name]


def unify_features(features):
    # extract features or combine features
    if isinstance(features, list):
        return FeatureUnion(features)
    return features


def test(model, X_test):
    pass


def report(target, pred):
    print('RMSE on testing, {:.2}'.format(np.sqrt(mean_squared_error(target, pred))))
    print('R2 on testing, {:.2}'.format(r2_score(target, pred)))


def train(X, Y, pipeline):
    pass


# def run_experiments(X, Y, X_test, Y_test, clf, feature_lst, feature_name):
#     print("=============================================")
#     print("Running experiment for %s" % feature_name)
#     print("=============================================")
#
#     # create pipeline 1.> feature extraction -> 2.> classifier/regression
#
#     pipeline = Pipeline([('all', all_features), ('clf', clf)])
#
#     # train
#     model = train(X, Y, pipeline)
#
#     # test
#     Y_predicted = test(model, X_test)
#
#     # print report
#     report(Y_test, Y_predicted)



def dev_test_folds(X_train):
    train_index = int(math.ceil(len(X_train) * 0.8))

    train_fold = [-1 for _ in range(train_index)]
    # print train_fold
    test_fold = [0 for _ in range(train_index, len(X_train))]
    # print test_fold
    return train_fold + test_fold


def plot_confusion_matrix(cm, title='Confusion matrix', cmap=plt.cm.Blues, target_names=[]):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(target_names))
    plt.xticks(tick_marks, target_names, rotation=45)
    plt.yticks(tick_marks, target_names)
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


if __name__ == "__main__":
    engine = create_engine('mysql://root:root@localhost:3306/ml_project_amazon_data?charset=utf8')
    Session = sessionmaker(bind=engine)
    session = Session()
    reviewers = ['A3AKD6UWETAXGE',
                 'A1V1LGKJMMAM30',
                 'AE820S4DM51RK',
                 'A33GGMOZAGLOX8',
                 'A2F6N60Z96CAJI',
                 ]
    # reviewers = loader.get_reviewers(session)
    for reviewer in reviewers:
        features_lst = [
            # Reviews
            ("swn", get_feature_extractor('swn')),
            ("review_unigram", get_feature_extractor('review_unigram')),
            ("reviews_bigram", get_feature_extractor('reviews_bigram')),
            ("reviews_trigram", get_feature_extractor('reviews_trigram')),
            ("reviews_uni_bi_tri_gram", get_feature_extractor('reviews_uni_bi_tri_gram')),
            ("reviews_char_bigram", get_feature_extractor('reviews_char_bigram')),
            ("reviews_char_tri", get_feature_extractor('reviews_char_tri')),
            ("reviews_char_3_5", get_feature_extractor('reviews_char_3_5')),
            ("reviews_char_bigram_word", get_feature_extractor('reviews_char_bigram_word')),
            ("reviews_char_tri_word", get_feature_extractor('reviews_char_tri_word')),
            ("reviews_char_3_5_word", get_feature_extractor('reviews_char_3_5_word')),
            ("review_writing_density", get_feature_extractor('review_writing_density')),
            ("review_info", get_feature_extractor('review_info')),

            # Books
            ("books_unigram", get_feature_extractor('books_unigram')),
            ("books_bigram", get_feature_extractor('books_bigram')),
            ("books_trigram", get_feature_extractor('books_trigram')),
            ("books_uni_bi_tri_gram", get_feature_extractor('books_uni_bi_tri_gram')),
            ("books_char_bigram", get_feature_extractor('books_char_bigram')),
            ("books_char_tri", get_feature_extractor('books_char_tri')),
            ("books_char_3_5", get_feature_extractor('books_char_3_5')),
            ("books_char_bigram_word", get_feature_extractor('books_char_bigram_word')),
            ("books_char_tri_word", get_feature_extractor('books_char_tri_word')),
            ("books_char_3_5_word", get_feature_extractor('books_char_3_5_word')),
            ("books_writing_density", get_feature_extractor('books_writing_density')),
            ("books_info", get_feature_extractor('books_info')),
            ("books_genre", get_feature_extractor('books_genre')),

        ]

        print ("Running Experiments for %s" % reviewer)
        print("===================================================================")
        print()
        X_train, Y_train, X_test, Y_test = loader.load_data(reviewer, session)
        # X_train=np.array([r.review_text for r in X_train])
        # X_test=np.array([r.review_text for r in X_test])
        # for l in X_train:
        #     print l
        #     print

        print (
            "len X train=%d, Y train=%d, X test=%d, Y test=%d" % (len(X_train), len(Y_train), len(X_test), len(Y_test)))
        # features
        # features = unify_features(
        #     [('review_unigram', get_feature_extractor('review_unigram'))])


        for name, fea in features_lst:
            print ("Running Experiments for feature %s" % name)
            print("===================================================================")

            # feature = get_feature_extractor('review_unigram')

            # # setup classifiers
            # classifiers = [
            #     ('linear regression', LinearRegression()),
            #     ('lasso()', Lasso()),
            #     ('elastic-net(.5)', ElasticNet(alpha=0.5)),
            #     ('lasso(.5)', Lasso(alpha=0.5)),
            #     ('ridge(.5)', Ridge(alpha=0.5)),
            # ]

            # run
            # for clf_name, clf in classifiers:

            clf = LogisticRegression(C=1.0)
            # clf = SVC(C=1.0)

            param_grid = {'clf__C': [1e-3, 1e-2, 1e-1, 1.0, 10, 100, 1000]}
            # param_grid = {'clf__C': [1e-3, 1e-2, 1e-1, 1.0, 10, 100, 1000],
            #               'clf__kernel': ['linear', 'poly', 'rbf', 'sigmoid', 'precomputed']}

            pipeline = Pipeline([('features', fea), ('clf', clf)])
            cv = PredefinedSplit(test_fold=dev_test_folds(X_train))

            # grid search for hyper parameter
            gs = grid_search.GridSearchCV(pipeline, param_grid, cv=cv, verbose=5)
            gs.fit(X_train, Y_train)

            # fit the best classifier
            best_clf = gs.best_estimator_.fit(X_train, Y_train)
            Y_predicted = best_clf.predict(X_test)
            # report(Y_predicted,Y_test)

            summary = best_clf.score(X_test, Y_test)
            print("===================================================================")

            print "Accuracy -> %.3f" % summary
            print("===================================================================")

            print classification_report(Y_test, Y_predicted)
            print("Precision Micro= %f  Macro= %f Weighted= %f " % (
                precision_score(Y_test, Y_predicted, average='micro'),
                precision_score(Y_test, Y_predicted, average='macro'),
                precision_score(Y_test, Y_predicted, average='weighted')))

            print("Recall Micro= %f  Macro= %f Weighted= %f " % (
                recall_score(Y_test, Y_predicted, average='micro'), recall_score(Y_test, Y_predicted, average='macro'),
                recall_score(Y_test, Y_predicted, average='weighted')))

            print("F1-Score Micro= %f  Macro= %f Weighted= %f " % (
                f1_score(Y_test, Y_predicted, average='micro'), f1_score(Y_test, Y_predicted, average='macro'),
                f1_score(Y_test, Y_predicted, average='weighted')))

            print("===================================================================")

            # Compute confusion matrix
            cm = confusion_matrix(Y_test, Y_predicted)
            np.set_printoptions(precision=2)
            print('Confusion matrix')
            print("===================================================================")

            print(cm)

            print ("Done Experiments for feature %s" % name)
            print("===================================================================")
            print("")
            print("")
            print("")
            print("")
            # plt.figure()
            # plot_confusion_matrix(cm)

        print ("Done Experiments for %s" % reviewer)
        print("===================================================================")
        print()
