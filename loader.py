# -*- coding: utf-8 -*-
from sklearn.cross_validation import train_test_split
from sqlalchemy import Column, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.mysql import VARCHAR, TEXT,INTEGER,FLOAT, DATETIME
# from sqlalchemy import Column, Integer, String, Float, Text, create_engine, Date, ForeignKey, Unicode, UnicodeText
from sqlalchemy.orm import sessionmaker, relationship, object_session
from sqlalchemy.sql import func
import numpy as np
import math
from sklearn import preprocessing


Base = declarative_base()


class Books(Base):
    __tablename__ = 'books'
    product_id = Column(VARCHAR(25), primary_key=True)
    product_title = Column(TEXT)
    hardcover_price = Column(FLOAT)
    number_of_reviews = Column(INTEGER)
    average_customer_rating = Column(INTEGER)
    percent_of_1star_reviews = Column(INTEGER)
    percent_of_2star_reviews = Column(INTEGER)
    percent_of_3star_reviews = Column(INTEGER)
    percent_of_4star_reviews = Column(INTEGER)
    percent_of_5star_reviews = Column(INTEGER)
    genre_ranking = Column(TEXT)
    description = Column(TEXT)
    size = Column(INTEGER)
    type = Column(VARCHAR(45))
    size_units = Column(VARCHAR(45))
    kindle_price = Column(FLOAT)
    paperback_price = Column(FLOAT)
    publication_date = Column(DATETIME)
    publisher = Column(TEXT)
    product_category = Column(VARCHAR(45))

    def __init__(self, product_id, product_title, hardcover_price, number_of_reviews, average_customer_rating,
                 percent_of_1star_reviews, percent_of_2star_reviews, percent_of_3star_reviews, percent_of_4star_reviews,
                 percent_of_5star_reviews, genre_ranking, description, size, type, size_units, kindle_price,
                 paperback_price, publication_date, publisher, product_category):
        self.product_id = product_id
        self.product_title = product_title
        self.hardcover_price = hardcover_price
        self.number_of_reviews = number_of_reviews
        self.average_customer_rating = average_customer_rating
        self.percent_of_1star_reviews = percent_of_1star_reviews
        self.percent_of_2star_reviews = percent_of_2star_reviews
        self.percent_of_3star_reviews = percent_of_3star_reviews
        self.percent_of_4star_reviews = percent_of_4star_reviews
        self.percent_of_5star_reviews = percent_of_5star_reviews
        self.genre_ranking = genre_ranking
        self.description = description
        self.size = size
        self.type = type
        self.size_units = size_units
        self.kindle_price = kindle_price
        self.paperback_price = paperback_price
        self.publication_date = publication_date
        self.publisher = publisher
        self.product_category = product_category


class Reviews(Base):
    __tablename__ = 'reviews'
    review_id = Column(VARCHAR(25), primary_key=True)
    product_id = Column(VARCHAR(25), ForeignKey("books.product_id"))
    customer_id = Column(VARCHAR(30))
    helpful_count = Column(INTEGER)
    out_of_helpful_count = Column(INTEGER)
    customer_review_rating = Column(FLOAT)
    review_written_date = Column(DATETIME)
    review_title = Column(TEXT)
    review_text = Column(TEXT)
    number_of_comments = Column(INTEGER)
    amazon_verified_purchase = Column(INTEGER)
    amazon_vine_program_review = Column(INTEGER)
    review_pos_tagged = Column(TEXT)
    product = relationship("Books")

    def __init__(self, id, product_id, customer_id, helpful_count, out_of_helpful_count, customer_review_rating,
                 review_text, review_title, number_of_comments, amazon_verified_purchase, amazon_vine_program_review,
                 review_pos_tagged, review_written_date):
        self.review_id = id
        self.product_id = product_id
        self.customer_id = customer_id
        self.helpful_count = helpful_count
        self.out_of_helpful_count = out_of_helpful_count
        self.customer_review_rating = customer_review_rating
        self.review_text = review_text
        self.review_title = review_title
        self.number_of_comments = number_of_comments
        self.amazon_verified_purchase = amazon_verified_purchase
        self.amazon_vine_program_review = amazon_vine_program_review
        self.review_pos_tagged = review_pos_tagged
        self.review_written_date = review_written_date


def get_reviewers(session):
    return [reviewer[0] for reviewer in session.query(Reviews.customer_id).distinct()]


def get_reviews(id, session):
    return [reviewer for reviewer in
            session.query(Reviews).filter(Reviews.customer_id == id).order_by(func.DATE(Reviews.review_written_date))]


def load_data(id, session):
    reviews = np.array(get_reviews(id, session))
    train_index = math.ceil(len(reviews) * 0.8)
    train, test = reviews[0:train_index], reviews[train_index:]
    y_train, y_test = [int(x.customer_review_rating) for x in train], [int(x.customer_review_rating) for x in test]
    le = preprocessing.LabelEncoder()
    le.fit(y_train)
    return train, le.transform(y_train), test, le.transform(y_test)


if __name__ == '__main__':
    # session = Session()
    # print get_reviewers(session)
    # for review in get_reviews(id='A1V1LGKJMMAM30', session=session):
    #     print review.review_id, review.review_text, review.review_written_date
    #     # for instance in session.query(Reviews).order_by(Reviews.review_id)[1:3]:
        #     print instance.review_id, instance.review_text
        #     print instance.product_id
        #     print instance.product.product_title
        #     print "================================"

    pass
