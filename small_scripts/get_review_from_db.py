import MySQLdb

__author__ = 'shrprasha'

conn = MySQLdb.connect("localhost", "root", "", "ml_project_amazon_data", charset='utf8')
cursor = conn.cursor()
dict_cursor = conn.cursor(MySQLdb.cursors.DictCursor)

count = 0

def get_all_reviews():
    query = "SELECT * " \
            "FROM reviews r " \
            "JOIN books b on b.product_id = r.product_id " \
            "limit 10"
    dict_cursor.execute(query)
    results = list(dict_cursor.fetchall())
    return results


if __name__ == "__main__":
    print get_all_reviews()

    conn.close()

